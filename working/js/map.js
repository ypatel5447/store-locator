function initMap() {

    // set default center as Wakefield store
    var centerLatLng =  {lat: 53.68492, lng: -1.49661 };

    var infoWindow = new google.maps.InfoWindow();
    var locationSelect;

	// create map and say which HTML element it should appear in
	var map = new google.maps.Map(document.getElementById('map'), {
		center: centerLatLng,
        zoom: 10,
        mapTypeId: 'roadmap',
    });

    // Get JSON from file and cycle through data creating markers on the map
    // Can be easily amended to call an api and return promise with .then()
    $.getJSON("stores.json", { crossdomain: true }, function(json) {
        $.each(json, function(key, data) {
            var latLng = new google.maps.LatLng(data.geo.latitude, data.geo.longitude);

            // Creating a marker and putting it on the map
            var marker = new google.maps.Marker({
                position: latLng,
                title: data.branch,
                icon: 'working/images/maps-icon.png'
            });
            marker.setMap(map);

            // Add window pop up displaying store info when clicked
            marker.addListener('click', function() {
                showStoreInfo(data);
                infoWindow.open(map, marker);
            });
        });
    });

    // show store info in box
    // additional content such as hours, address, phone number, etc. can be easily added here
    // this can be split out into a view as it expands
    function showStoreInfo(store) {
        infoWindow.setContent('<div class="info-window"> <h3>' + store.branch + '</h3></div>');
    }

    // Store-locator min js function called here with possible additional callbacks
    // $(function() {
    //     $('#store-locator').storeLocator();
    // });
}