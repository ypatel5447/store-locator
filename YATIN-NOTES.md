#Notes - Yatin Patel

#Steps taken
Created a JSON static file (stores.json) and created my own test data based on the example given in the README (3 stores manually added)
Added HTML markup in index.js in the location specified for the map to be displayed
Enabled API's in Google Cloud platform
Used Google Maps API to generate map and create markers based on JSON data of stores

#Completed work towards finished project
Visuals added for postcode search and current location
Maps displayed with API key and JSON data being pulled in and displayed successfully
Information window displaying on click within map

#Comments
Noticed the store-locator-min.js included in the source files.  Can be used to calculate to complete project and find distances between stores and display output
Use of PSD to pull in correct assets/fonts/colours as shown in designs - no Photoshop installed so used https://www.photopea.com/ as a temp solution
Design of maps is relatively responsive - font size changes and other amendments can be obviously made


#Future - if further time
Create a list of existing stores below map that can be brought up if user needs - output of stores sorted by distance of current location
Maps can be styled much further - custom CSS - shown one way with marker.setIcon to show blue icon as per the PSD
CSS Media queries can be used to to be more intuitive on different devices/screen widths - one example given at bottom of css using screen width


Total approximate time taken so far: 3.5 hours